<?php

namespace credy\leakyBucket\handlers;

class ArrayHandler extends ExceptionHandler
{
    /**
     * @var array
     */
    protected $excepitonTimestamps = [];

    /**
     * @var float
     */
    protected $suspendUntil = 0;

    /**
     * @var int
     */
    protected $suspendedCount = 0;

    protected function getSuspendCount($key)
    {
        return $this->suspendedCount;
    }

    public function isSuspended($key)
    {
        if ($this->getUtime() >= $this->suspendUntil) {
            return false;
        }
        return true;
    }

    protected function increaseSuspendCount(string $key, int $suspendDuration, int $exceptionInterval, int $suspendCount)
    {
        $this->suspendedCount++;
    }

    protected function innerSuspend($key, $suspendDuration)
    {
        $this->suspendUntil = $this->getUtime() + $suspendDuration;
    }

    protected function innerUnsuspend($key)
    {
        $this->suspendUntil = $this->getUtime();
    }

    protected function addException($key, $exceptionInterval)
    {
        if ($this->getUtime() > $this->suspendUntil + $exceptionInterval) {
            $this->suspendedCount = 0;
        }
        $this->excepitonTimestamps[] = $this->getUtime();
    }

    protected function getExceptionCount(string $key, int $exceptionInterval)
    {
        $expiredTime = $this->getUtime() + $exceptionInterval;
        $this->excepitonTimestamps = array_filter($this->excepitonTimestamps, function ($timestamp) use ($expiredTime) {
            return $timestamp < $expiredTime;
        });
        return count($this->excepitonTimestamps);
    }
}
