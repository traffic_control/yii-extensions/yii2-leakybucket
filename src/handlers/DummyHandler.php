<?php

namespace credy\leakyBucket\handlers;

class DummyHandler extends ExceptionHandler
{
    protected function innerUnsuspend(string $key)
    {
    }

    /**
     * @inheritdoc
     */
    protected function getSuspendCount(string $key)
    {
        return 0;
    }

    protected function increaseSuspendCount(string $key, int $suspendDuration, int $exceptionInterval, int $suspendCount)
    {
    }

    protected function innerSuspend(string $key, int $suspendDuration)
    {
    }

    /**
     * @inheritdoc
     */
    public function isSuspended(string $key)
    {
        return false;
    }

    protected function addException(string $key, int $exceptionInterval)
    {
    }

    /**
     * @inheritdoc
     */
    protected function getExceptionCount(string $key, int $exceptionInterval)
    {
        return 0;
    }
}
