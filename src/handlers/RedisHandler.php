<?php

namespace credy\leakyBucket\handlers;

use credy\leakyBucket\handlers\ExceptionHandler as BaseExceptionHandler;
use yii\di\Instance;
use yii\redis\Connection;

class RedisHandler extends BaseExceptionHandler
{
    /**
     * @var Connection|string|array
     */
    public $redis = 'redis';

    public function init()
    {
        $this->redis = Instance::ensure($this->redis, Connection::class);
        parent::init();
    }

    protected function innerUnsuspend(string $key)
    {
        $this->redis->del($this->getSuspendedKey($key));
    }

    /**
     * @inheritdoc
     */
    public function isSuspended(string $key)
    {
        if ($this->redis->exists($this->getSuspendedKey($key))) {
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    protected function getSuspendCount(string $key)
    {
        return $this->redis->get($this->getSuspendedCountKey($key)) ?: 0;
    }

    protected function increaseSuspendCount(string $key, int $suspendDuration, int $exceptionInterval, int $suspendCount)
    {
        $this->redis->setex($this->getSuspendedCountKey($key), $exceptionInterval + $suspendDuration, $suspendCount + 1);
    }

    /**
     * @inheritdoc
     */
    protected function getExceptionCount(string $key, int $exceptionInterval)
    {
        $this->redis->zremrangebyscore($this->getExceptionsKey($key), '0', $this->getUtime() - $exceptionInterval);

        return $this->redis->zlexcount($this->getExceptionsKey($key), '-', '+');
    }

    protected function addException(string $key, int $exceptionInterval)
    {
        $this->redis->zadd($this->getExceptionsKey($key), $this->getUtime(), $this->getUtime());
    }

    protected function innerSuspend(string $key, int $suspendDuration)
    {
        $this->redis->setex($this->getSuspendedKey($key), $suspendDuration, 'suspended');
    }

    protected function getSuspendedKey($key)
    {
        return implode(':', [__CLASS__, __METHOD__, $key, 'isSuspended']);
    }

    protected function getSuspendedCountKey($key)
    {
        return implode(':', [__CLASS__, __METHOD__, $key, 'suspendedCount']);
    }

    protected function getExceptionsKey($key)
    {
        return implode(':', [__CLASS__, __METHOD__, $key, 'exceptions']);
    }
}
