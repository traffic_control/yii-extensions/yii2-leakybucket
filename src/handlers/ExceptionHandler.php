<?php

namespace credy\leakyBucket\handlers;

use credy\leakyBucket\events\SuspendEvent;
use Throwable;
use yii\base\Component;

abstract class ExceptionHandler extends Component
{
    /**
     * @var int
     */
    public $suspendDuration = 600;

    /**
     * @var int
     */
    public $exceptionThreshold = 10;

    /**
     * @var int
     */
    public $exceptionInterval = 600;

    /**
     * @var int
     */
    public $durationIncrement = 0;

    /**
     * @return bool
     */
    abstract public function isSuspended(string $key);

    /**
     * @return int
     */
    abstract protected function getSuspendCount(string $key);

    abstract protected function increaseSuspendCount(string $key, int $suspendDuration, int $exceptionInterval, int $suspendCount);

    /**
     * @param int $suspendDuration
     */
    abstract protected function innerSuspend(string $key, int $suspendDuration);

    abstract protected function innerUnsuspend(string $key);

    /**
     * @return int
     */
    abstract protected function getExceptionCount(string $key, int $exceptionInterval);

    /**
     * @param Throwable $throwable
     *
     * @return void
     */
    abstract protected function addException(string $key, int $exceptionInterval);

    /**
     * @param string $key unique key
     * @param callable $callback
     * @param mixed $defaultValue default return value
     * @param int $suspendDuration number of seconds for the callback to be suspended
     * @param int $exceptionThreshold amount of exceptions within $exceptionInterval required to suspend the callback
     * @param int $exceptionInterval interval in seconds in whcih exceptions are recorded
     * @param int $durationIncrement number of seconds to increase suspend duration after consecutive suspends
     *
     * @return mixed
     */
    public function try(
        string $key,
        callable $callback,
        $defaultValue = null,
        int $suspendDuration = null,
        int $exceptionThreshold = null,
        int $exceptionInterval = null,
        int $durationIncrement = null
    ) {
        if ($this->isSuspended($key)) {
            return $defaultValue;
        }

        try {
            return call_user_func($callback);
        } catch (Throwable $error) {
            $suspendDuration = $suspendDuration === null ? $this->suspendDuration : $suspendDuration;
            $exceptionThreshold = $exceptionThreshold === null ? $this->exceptionThreshold : $exceptionThreshold;
            $exceptionInterval = $exceptionInterval === null ? $this->exceptionInterval : $exceptionInterval;
            $durationIncrement = $durationIncrement === null ? $this->durationIncrement : $durationIncrement;

            $this->handleException($key, $suspendDuration, $exceptionThreshold, $exceptionInterval, $durationIncrement);

            throw $error;
        }
    }

    /**
     * @return int
     */
    protected function suspend(string $key, int $suspendDuration, int $exceptionInterval, int $durationIncrement, int $exceptionThreshold)
    {
        $suspendCount = $this->getSuspendCount($key);
        $suspendDuration = $suspendDuration + ($suspendCount * $durationIncrement);
        $this->innerSuspend($key, $suspendDuration);
        $this->increaseSuspendCount($key, $suspendDuration, $exceptionInterval, $suspendCount);

        $this->trigger(SuspendEvent::EVENT_SUSPEND, new SuspendEvent([
            'suspendCount' => $suspendCount,
            'exceptionThreshold' => $exceptionThreshold,
            'suspendDuration' => $suspendDuration,
        ]));

        return $suspendDuration;
    }

    public function unsuspend($key)
    {
        $this->innerUnsuspend($key);
    }

    /**
     *
     * @param string $key
     * @param int $suspendDuration
     * @param int $exceptionThreshold
     * @param int $exceptionInterval
     * @param int $durationIncrement
     *
     * @return false|int
     */
    public function handleException(string $key, int $suspendDuration = null, int $exceptionThreshold = null, int $exceptionInterval = null, int $durationIncrement = null)
    {
        $suspendDuration = $suspendDuration === null ? $this->suspendDuration : $suspendDuration;
        $exceptionThreshold = $exceptionThreshold === null ? $this->exceptionThreshold : $exceptionThreshold;
        $exceptionInterval = $exceptionInterval === null ? $this->exceptionInterval : $exceptionInterval;
        $durationIncrement = $durationIncrement === null ? $this->durationIncrement : $durationIncrement;

        $this->addException($key, $exceptionInterval);

        if (!$this->isSuspended($key) && ($this->getExceptionCount($key, $exceptionInterval) >= $exceptionThreshold)) {
            return $this->suspend($key, $suspendDuration, $exceptionInterval, $durationIncrement, $exceptionThreshold);
        }

        return false;
    }

    /**
     * For testing purposes
     *
     * @return string|float
     */
    protected function getUtime()
    {
        return microtime(true);
    }
}
