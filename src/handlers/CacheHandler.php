<?php

namespace credy\leakyBucket\handlers;

use yii\caching\Cache;
use yii\di\Instance;
use yii\helpers\ArrayHelper;

class CacheHandler extends ExceptionHandler
{
    /**
     * @var Cache|string|array
     */
    public $cache = 'cache';

    public function init()
    {
        $this->cache = Instance::ensure($this->cache, Cache::class);
        parent::init();
    }

    protected function innerUnsuspend($key)
    {
        $this->cache->delete($this->getSuspendedKey($key));
    }

    /**
     * @inheritdoc
     */
    public function isSuspended($key)
    {
        if ($this->cache->exists($this->getSuspendedKey($key))) {
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    protected function getSuspendCount($key)
    {
        return $this->cache->get($this->getSuspendedCountKey($key)) ?: 0;
    }

    protected function increaseSuspendCount(string $key, int $suspendDuration, int $exceptionInterval, int $suspendCount)
    {
        $this->cache->set($this->getSuspendedCountKey($key), $suspendCount + 1, $suspendDuration + $exceptionInterval);
    }

    /**
     * @inheritdoc
     */
    protected function getExceptionCount($key, int $exceptionInterval)
    {
        return count($this->getExceptions($key, $exceptionInterval));
    }

    protected function getExceptions($key, $exceptionInterval)
    {
        $exceptions = (array) $this->cache->get($this->getExceptionsKey($key));
        return $this->filterExceptions($exceptions, $exceptionInterval);
    }

    protected function addException($key, int $exceptionInterval)
    {
        $exceptions = $this->getExceptions($key, 0);

        $exceptions[] = [$this->getUtime()];
        $this->setExceptions($key, $exceptions);
    }

    protected function setExceptions($key, $exceptions)
    {
        $this->cache->set($this->getExceptionsKey($key), $exceptions);
    }

    protected function innerSuspend($key, $suspendDuration)
    {
        $this->cache->set($this->getSuspendedKey($key), true, $suspendDuration);
    }

    protected function getSuspendedKey($key)
    {
        return implode(':', [__CLASS__, __METHOD__, $key, 'isSuspended']);
    }

    protected function getSuspendedCountKey($key)
    {
        return implode(':', [__CLASS__, __METHOD__, $key, 'suspendedCount']);
    }

    protected function getExceptionsKey($key)
    {
        return implode(':', [__CLASS__, __METHOD__, $key, 'exceptions']);
    }

    protected function filterExceptions(array $exceptions, $exceptionInterval)
    {
        foreach ($exceptions as $key => $exceptionData) {
            if (!(ArrayHelper::getValue($exceptionData, 0) + ($exceptionInterval) > $this->getUtime())) {
                unset($exceptions[$key]);
            }
        }
        return $exceptions;
    }
}
