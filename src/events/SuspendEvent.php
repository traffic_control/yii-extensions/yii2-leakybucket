<?php

namespace credy\leakyBucket\events;

use yii\base\Event;

class SuspendEvent extends Event
{
    const EVENT_SUSPEND = 'suspend';

    public $suspendCount;

    public $exceptionThreshold;

    public $suspendDuration;
}
