Under development
------------------------

1.0.1 2024-07-05
------------------------
- Bugfix: Fix incrementing suspend duration (Kalmer)

1.0.0 2024-06-10
------------------------
- Feature: Initial implementation (Kalmer)

