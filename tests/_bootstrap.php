<?php

define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);
require_once __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../vendor/autoload.php';

Yii::setAlias('@tests', __DIR__);

$config = [
    'id' => 'app-test',
    'basePath' => dirname(__DIR__),
    'components' => [
        'redis' => [
            'class' => yii\redis\Connection::class,
            'hostname' => 'redis'
        ],
        'cache' => [
            'class' => yii\caching\FileCache::class,
            'cachePath' => '@tests/runtime/cache',
        ],
    ],
];

(new yii\web\Application($config));
