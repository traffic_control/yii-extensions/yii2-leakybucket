<?php

namespace tests;

use Codeception\Stub\Expected;
use credy\leakyBucket\handlers\ExceptionHandler;
use Exception;
use yii\helpers\ArrayHelper;

/**
 * Class BaseHandlerTest
 *
 * @package tests\unit
 */
class BaseHandlerTest extends \Codeception\Test\Unit
{
    /**
     * @var ExceptionHandler
     */
    protected $handlerClass;

    public function testAddException()
    {
        $handler = $this->getHandler([
            'addException' => Expected::once()
        ]);

        $handler->handleException('test key');
    }

    public function testSuspend()
    {
        $handler = $this->getHandler([
            'innerSuspend' => Expected::once(),
            'exceptionThreshold' => 1,
        ]);

        $handler->handleException('test key');
    }

    public function testIncreaseSuspendCount()
    {
        $handler = $this->getHandler([
            'increaseSuspendCount' => Expected::once(),
            'exceptionThreshold' => 1,
        ]);

        $handler->handleException('test key');
    }

    public function testIsSuspended()
    {
        $handler = $this->getHandler([
            'exceptionThreshold' => 1,
        ]);

        $this->assertFalse($handler->isSuspended('test key'));
        $handler->handleException('test key');

        $this->assertTrue($handler->isSuspended('test key'));
    }

    public function testInnerUnsuspend()
    {
        $handler = $this->getHandler([
            'innerUnsuspend' => Expected::once(),
            'exceptionThreshold' => 1,
        ]);

        $this->assertFalse($handler->isSuspended('test key'));
        $handler->handleException('test key');
        $this->assertTrue($handler->isSuspended('test key'));

        $handler->unsuspend('test key');
    }

    public function testUnsuspend()
    {
        $handler = $this->getHandler([
            'exceptionThreshold' => 1,
        ]);

        $this->assertFalse($handler->isSuspended('test key'));
        $handler->handleException('test key');
        $this->assertTrue($handler->isSuspended('test key'));

        $handler->unsuspend('test key');
        $this->assertFalse($handler->isSuspended('test key'));
    }

    public function testTry()
    {
        $leakyBucket = $this->getHandler();

        $response = $leakyBucket->try(
            'test',
            function () {
                return true;
            }
        );

        $this->assertTrue($response);
    }

    public function testTryWithException()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Test');

        $leakyBucket = $this->getHandler([
            'addException' => Expected::once()
        ]);

        $leakyBucket->try(
            'test',
            function () {
                throw new Exception('Test');
            }
        );
    }

    public function testTrySuspend()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Test');

        $leakyBucket = $this->getHandler([
            'isSuspended' => false,
            'getExceptionCount' => 10,
            'exceptionThreshold' => 10,
            'innerSuspend' => Expected::once(),
        ]);

        $leakyBucket->try(
            'test',
            function () {
                throw new Exception('Test');
            }
        );
    }

    public function testTrySuspended()
    {
        $leakyBucket = $this->getHandler([
            'isSuspended' => true,
            'addException' => Expected::never(),
        ]);

        $response = $leakyBucket->try(
            'test',
            function () {
                throw new Exception('Test');
            },
            'default value'
        );

        $this->assertEquals('default value', $response);
    }

    public function testTryNoDefaultValueOnException()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Test');

        $leakyBucket = $this->getHandler([
            'isSuspended' => false,
            'addException' => Expected::once(),
        ]);

        $response = $leakyBucket->try(
            'test',
            function () {
                throw new Exception('Test');
            },
            'default value'
        );

        $this->assertNotEquals('default value', $response);
    }


    public function testTryIncreaseSuspendCount()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Test');

        $leakyBucket = $this->getHandler([
            'isSuspended' => false,
            'getExceptionCount' => 10,
            'exceptionThreshold' => 10,
            'increaseSuspendCount' => Expected::once(),
        ]);

        $leakyBucket->try(
            'test',
            function () {
                throw new Exception('Test');
            }
        );
    }

    public function testTryExceptionCountBelowThreshold()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Test');

        $leakyBucket = $this->getHandler([
            'getExceptionCount' => 8,
            'exceptionThreshold' => 10,
            'increaseSuspendCount' => Expected::never(),
        ]);

        $leakyBucket->try(
            'test',
            function () {
                throw new Exception('Test');
            }
        );
    }

    public function testTryExceptionCountAboveThreshold()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Test');

        $leakyBucket = $this->getHandler([
            'getExceptionCount' => 8,
            'exceptionThreshold' => 6,
            'increaseSuspendCount' => Expected::once(),
        ]);

        $leakyBucket->try(
            'test',
            function () {
                throw new Exception('Test');
            }
        );
    }

    protected function getHandler($config = [])
    {
        return $this->make($this->handlerClass, ArrayHelper::merge([
            'suspendDuration' => 600,
            'exceptionThreshold' => 10,
            'durationIncrement' => 0,
            'exceptionInterval' => 600,
        ], $config));
    }
}
