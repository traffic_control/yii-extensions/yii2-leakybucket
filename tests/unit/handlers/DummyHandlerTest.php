<?php

namespace tests\unit\handlers;

use credy\leakyBucket\handlers\DummyHandler;
use Exception;

/**
 * Class DummyHandlerTest
 *
 * @package tests\unit\handlers
 */
class DummyHandlerTest extends \Codeception\Test\Unit
{
    public function testUnsuspend()
    {
        $handler = new DummyHandler();

        $handler->unsuspend('test key');
        $this->assertFalse($handler->isSuspended('test key'));
    }
}
