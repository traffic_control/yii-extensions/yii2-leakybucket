<?php

namespace tests\unit\handlers;

use Codeception\Stub;
use Codeception\Stub\Expected;
use credy\leakyBucket\handlers\RedisHandler;
use Exception;
use PHPUnit\Framework\MockObject\Stub\ReturnCallback;
use tests\BaseHandlerTest;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\redis\Connection;

/**
 * Class RedisHandlerTest
 *
 * @package tests\unit\handlers
 */
class RedisHandlerTest extends BaseHandlerTest
{
    protected $handlerClass = RedisHandler::class;

    public function _after()
    {
        parent::_after();
        Yii::$app->redis->flushdb();
    }

    public function testInitWithWrongRedis()
    {
        $this->expectException(InvalidConfigException::class);

        new RedisHandler([
            'redis' => new BaseObject(),
        ]);
    }

    public function testAddException()
    {
        $handler = new RedisHandler([
            'redis' => $this->make(Connection::class, [
                'executeCommand' => Expected::exactly(4, Stub::consecutive(
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('ZADD', $name);
                        $this->assertStringContainsString('exceptions', $params[0]);
                        return 'test';
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('EXISTS', $name);
                        $this->assertStringContainsString('isSuspended', $params[0]);
                        return false;
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('ZREMRANGEBYSCORE', $name);
                        $this->assertStringContainsString('exceptions', $params[0]);
                        return 'test';
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('ZLEXCOUNT', $name);
                        $this->assertStringContainsString('exceptions', $params[0]);
                        return 1;
                    }),
                )),
            ]),
            'suspendDuration' => 600,
            'exceptionThreshold' => 10,
            'durationIncrement' => 0,
            'exceptionInterval' => 600,
        ]);

        $handler->handleException(new Exception('test exception'));
    }

    public function testSuspend()
    {
        $handler = $this->make(RedisHandler::class, [
            'redis' => $this->make(Connection::class, [
                'executeCommand' => Expected::exactly(7, Stub::consecutive(
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('ZADD', $name);
                        $this->assertStringContainsString('exceptions', $params[0]);
                        return 'test';
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('EXISTS', $name);
                        $this->assertStringContainsString('isSuspended', $params[0]);
                        return false;
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('ZREMRANGEBYSCORE', $name);
                        $this->assertStringContainsString('exceptions', $params[0]);
                        return 'test';
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('ZLEXCOUNT', $name);
                        $this->assertStringContainsString('exceptions', $params[0]);
                        return 10;
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('GET', $name);
                        $this->assertStringContainsString('suspendedCount', $params[0]);
                        return false;
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('SETEX', $name);
                        $this->assertStringContainsString('isSuspended', $params[0]);
                        $this->assertEquals(500, $params[1]);
                        return false;
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('SETEX', $name);
                        $this->assertStringContainsString('suspendedCount', $params[0]);
                        $this->assertEquals(1100, $params[1]);
                        return false;
                    }),
                )),
            ]),
            'suspendDuration' => 500,
            'exceptionThreshold' => 10,
            'durationIncrement' => 10,
            'exceptionInterval' => 600,
        ]);

        $handler->handleException(new Exception('test exception'));
    }

    public function testIncreaseSuspendCount()
    {
        $handler = $this->make(RedisHandler::class, [
            'redis' => $this->make(Connection::class, [
                'executeCommand' => Expected::exactly(7, Stub::consecutive(
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('ZADD', $name);
                        $this->assertStringContainsString('exceptions', $params[0]);
                        return 'test';
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('EXISTS', $name);
                        $this->assertStringContainsString('isSuspended', $params[0]);
                        return false;
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('ZREMRANGEBYSCORE', $name);
                        $this->assertStringContainsString('exceptions', $params[0]);
                        return 'test';
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('ZLEXCOUNT', $name);
                        $this->assertStringContainsString('exceptions', $params[0]);
                        return 10;
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('GET', $name);
                        $this->assertStringContainsString('suspendedCount', $params[0]);
                        return 2;
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('SETEX', $name);
                        $this->assertStringContainsString('isSuspended', $params[0]);
                        $this->assertEquals(520, $params[1]);
                        return false;
                    }),
                    new ReturnCallback(function ($name, $params = []) {
                        $this->assertEquals('SETEX', $name);
                        $this->assertStringContainsString('suspendedCount', $params[0]);
                        $this->assertEquals(1120, $params[1]);
                        $this->assertEquals(3, $params[2]);
                        return false;
                    }),
                )),
            ]),
            'suspendDuration' => 500,
            'exceptionThreshold' => 10,
            'durationIncrement' => 10,
            'exceptionInterval' => 600,
        ]);

        $handler->handleException('test key');
    }

    protected function getHandler($config = [])
    {
        return parent::getHandler(ArrayHelper::merge([
            'redis' => Yii::$app->redis,
        ], $config));
    }
}
