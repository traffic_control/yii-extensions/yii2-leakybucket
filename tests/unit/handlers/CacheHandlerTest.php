<?php

namespace tests\unit\handlers;

use Codeception\Stub;
use Codeception\Stub\Expected;
use credy\leakyBucket\handlers\CacheHandler;
use PHPUnit\Framework\MockObject\Stub\ReturnCallback;
use tests\BaseHandlerTest;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\caching\DummyCache;
use yii\caching\FileCache;
use yii\helpers\ArrayHelper;

/**
 * Class CacheHandlerTest
 *
 * @package tests\unit\handlers
 */
class CacheHandlerTest extends BaseHandlerTest
{
    protected $handlerClass = CacheHandler::class;

    public function _after()
    {
        parent::_after();
        Yii::$app->cache->flush();
    }

    public function testInitWithWrongCache()
    {
        $this->expectException(InvalidConfigException::class);

        new CacheHandler([
            'cache' => new BaseObject(),
        ]);
    }

    public function testInitWithRightCache()
    {
        $this->assertNotNull(new CacheHandler([
            'cache' => new FileCache(),
        ]));
    }

    public function testIncreaseSuspendCountWithDuration()
    {
        $handler = $this->make(CacheHandler::class, [
            'cache' => $this->make(DummyCache::class, [
                'set' => Expected::exactly(2, Stub::consecutive(
                    new ReturnCallback(function ($key, $value, $duration) {
                        $this->assertStringContainsString('isSuspended', $key);
                        $this->assertTrue($value);
                        $this->assertEquals(510, $duration);
                        return;
                    }),
                    new ReturnCallback(function ($key, $value, $duration) {
                        $this->assertStringContainsString('suspendedCount', $key);
                        $this->assertEquals(2, $value);
                        $this->assertEquals(1110, $duration);
                        return;
                    }),
                )),
            ]),
            'isSuspended' => false,
            'getSuspendCount' => 1,
            'getExceptions' => [],
            'getExceptionCount' => 1,
            'setExceptions' => null,
            'suspendDuration' => 500,
            'exceptionThreshold' => 1,
            'durationIncrement' => 10,
            'exceptionInterval' => 600,
        ]);

        $handler->handleException('test key');
    }

    protected function getHandler($config = [])
    {
        return parent::getHandler(ArrayHelper::merge([
            'cache' => Yii::$app->cache,
        ], $config));
    }
}
