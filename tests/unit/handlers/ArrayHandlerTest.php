<?php

namespace tests\unit\handlers;

use Codeception\Stub;
use Codeception\Stub\Expected;
use credy\leakyBucket\handlers\ArrayHandler;
use Exception;
use tests\BaseHandlerTest;

/**
 * Class ArrayHandlerTest
 *
 * @package tests\unit\handlers
 */
class ArrayHandlerTest extends BaseHandlerTest
{
    protected $handlerClass = ArrayHandler::class;

    public function testIsSuspendedExpired()
    {
        $handler = $this->getHandler([
            'getUtime' => Expected::exactly(6, Stub::consecutive(
                microtime(true),
                microtime(true),
                microtime(true),
                microtime(true),
                microtime(true),
                microtime(true) + 1101,
            )),
            'suspendDuration' => 500,
            'exceptionThreshold' => 1,
        ]);

        $handler->handleException('test key');

        $this->assertFalse($handler->isSuspended('test key'));
    }
}
