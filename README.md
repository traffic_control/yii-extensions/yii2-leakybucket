# Yii2 LeakyBucket
===========

## Configuration:
### LeakyBucket Component:

Put following snippet into your config

```php
return [
    'components' => [
        'leakyBucket' => [
            'class' => credy\leakyBucket\handlers\DummyHandler::class,
        ]
    ]
];
```


## Usage

In your class:
```php
class YourClassName extends BaseObject
{
    public function yourMethod()
    {
        Yii::$app->leakyBucket->try(
            'unique key',
            function () {
                //insert your code here
            }
        );
    }
}
```

